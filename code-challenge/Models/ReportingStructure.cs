﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace challenge.Models
{
    public class ReportingStructure
    {
        //The employee
        public Employee employee { get; set; }
        //The number of employees who report to said employee
        public int numberOfReports { get; set; }

        
        public ReportingStructure(Employee ee)
        {
            employee = ee;
        }


        public void SetNumberOfReports()
        {
             numberOfReports = GetNumberOfReports(employee);
        }
        /// <summary>
        /// DISCLAIMER: I am assuming that all reporting structure are created such that if A reports 
        /// to B reports to C, C Can't Report to A.
        /// I am also assuming that Eventually an employee will have no direct reports.
        /// If this is false then this code will break. 
        /// </summary>
        /// <param name="ee">The Employee you are creating the report for</param>
        /// <returns>The int after recursion</returns>
        public int GetNumberOfReports(Employee ee)
        {
            int i = 0;
            if (ee.DirectReports != null)
            {
                foreach (Employee nextEE in ee.DirectReports)
                {
                    i += GetNumberOfReports(nextEE);
                }

                i += ee.DirectReports.Count();
            }
            return i;
        }
    }
}
