﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace challenge.Models
{
    public class CompensationAddModel
    {
        public DateTime EffectiveDate { get; set; }
        public decimal Salary { get; set; }
        public String EmployeeId { get; set; }
    }
}
