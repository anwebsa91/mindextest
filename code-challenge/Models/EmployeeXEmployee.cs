﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace challenge.Models
{
    public class EmployeeXEmployee
    {
        public String EmployeeXEmployeeId { get; set; }
        public String EmployeeId { get; set; }
        public String ReportsToId { get; set; }
    }
}
