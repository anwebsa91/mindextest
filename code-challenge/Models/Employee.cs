﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace challenge.Models
{
    public class Employee
    {
        public String EmployeeId { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Position { get; set; }
        public String Department { get; set; }
        public List<Employee> DirectReports { get; set; }


        public Employee()
        {
        }
        public Employee(EmployeeAddModel model)
        {
            EmployeeId = model.EmployeeId;
            FirstName = model.FirstName;
            LastName = model.LastName;
            Position = model.Position;
            Department = model.Department;
        }
    }


}
