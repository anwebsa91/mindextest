﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace challenge.Models
{
    public class Compensation
    {
        public String CompensationId { get; set; }
        public DateTime EffectiveDate { get; set; }
        public decimal Salary { get; set; }
        public String EmployeeId{ get; set; }
        public Employee Employee { get; set; }
        
        public Compensation()
        {

        }
        public Compensation(CompensationAddModel comp)
        {
            EffectiveDate = comp.EffectiveDate;
            EmployeeId = comp.EmployeeId;
            Salary = comp.Salary;
        }
    }
}
