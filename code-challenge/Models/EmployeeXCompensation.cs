﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace challenge.Models
{
    public class EmployeeXCompensation
    {
        public String EmployeeXCompensationId { get; set; }
        public String EmployeeId { get; set; }
        public String CompensationId { get; set; }
    }
}
