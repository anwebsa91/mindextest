﻿using challenge.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace challenge.Data
{
    public class EmployeeDataSeeder
    {
        private EmployeeContext _employeeContext;
        private const String EMPLOYEE_SEED_DATA_FILE = "resources/EmployeeSeedData.json";

        public EmployeeDataSeeder(EmployeeContext employeeContext)
        {
            _employeeContext = employeeContext;
        }

        public async Task Seed()
        {
            if(!_employeeContext.Employees.Any())
            {
                List<Employee> employees = LoadEmployees();
                _employeeContext.Employees.AddRange(employees);
                List<EmployeeXEmployee> references = SaveReferences(employees);
                _employeeContext.EmployeeXEmployees.AddRange(references);
                await _employeeContext.SaveChangesAsync();
            }
        }

        private List<Employee> LoadEmployees()
        {
            using (FileStream fs = new FileStream(EMPLOYEE_SEED_DATA_FILE, FileMode.Open))
            using (StreamReader sr = new StreamReader(fs))
            using (JsonReader jr = new JsonTextReader(sr))
            {
                JsonSerializer serializer = new JsonSerializer();

                List<Employee> employees = serializer.Deserialize<List<Employee>>(jr);
                FixUpReferences(employees);

                return employees;
            }
        }

        private void FixUpReferences(List<Employee> employees)
        {
            var employeeIdRefMap = from employee in employees
                                select new { Id = employee.EmployeeId, EmployeeRef = employee };

            employees.ForEach(employee =>
            {
                
                if (employee.DirectReports != null)
                {
                    var referencedEmployees = new List<Employee>(employee.DirectReports.Count);
                    employee.DirectReports.ForEach(report =>
                    {
                        var referencedEmployee = employeeIdRefMap.First(e => e.Id == report.EmployeeId).EmployeeRef;
                        referencedEmployees.Add(referencedEmployee);
                    });
                    employee.DirectReports = referencedEmployees;
                }
            });
        }


        /// <summary>
        /// This saves the references as an X Reference table string to string because persistence won't allow a list
        /// </summary>
        private List<EmployeeXEmployee> SaveReferences(List<Employee> employees)
        {
            List<EmployeeXEmployee> xEmployees = new List<EmployeeXEmployee>();
            foreach (var ee in employees)
            {
                if (ee.DirectReports != null)
                {
                    foreach (var suboordinate in ee.DirectReports)
                    {
                        EmployeeXEmployee employeeX = new EmployeeXEmployee();
                        employeeX.EmployeeXEmployeeId = Guid.NewGuid().ToString();
                        employeeX.EmployeeId = suboordinate.EmployeeId;
                        employeeX.ReportsToId = ee.EmployeeId;
                        xEmployees.Add(employeeX);
                    }
                }
            }
            return xEmployees;
        }
    }
}
