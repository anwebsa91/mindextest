﻿using challenge.Models;
using challenge.Repositories;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace challenge.Services
{
    public class CompensationService : ICompensationService
    {
        private readonly ICompensationRepository _compensationRepository;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly ILogger<CompensationService> _logger;

        public CompensationService(ILogger<CompensationService> logger, ICompensationRepository compensationRepository, IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
            _compensationRepository = compensationRepository;
            _logger = logger;
        }

        public Compensation Create(CompensationAddModel compensation)
        {
            Compensation comp = new Compensation(compensation);
            var newComp = _compensationRepository.Create(comp);
            _compensationRepository.CreateRelationship(newComp);
            _compensationRepository.SaveAsync().Wait();
            comp.Employee = _employeeRepository.GetById(compensation.EmployeeId);
            return comp;
        }

        public Compensation GetById(string id)
        {
            var compensation = _compensationRepository.GetById(id);
            compensation.Employee = _employeeRepository.GetById(compensation.EmployeeId);
            
            return compensation;
        }
    }
}
