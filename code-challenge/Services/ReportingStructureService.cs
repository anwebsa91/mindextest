﻿using challenge.Models;
using challenge.Repositories;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace challenge.Services
{
    public class ReportingStructureService : IReportingStructureService
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly ILogger<ReportingStructureService> _logger;

        public ReportingStructureService(ILogger<ReportingStructureService> logger, IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
            _logger = logger;
        }

        public ReportingStructure GetReportingStructure(String employeeId)
        {
            Employee employee = _employeeRepository.GetByIdWithReportingStructure(employeeId);
            ReportingStructure reportingStructure = new ReportingStructure(employee);
            reportingStructure.SetNumberOfReports();
            return reportingStructure;
        }
    }
}
