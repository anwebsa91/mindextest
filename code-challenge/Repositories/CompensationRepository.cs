﻿using challenge.Data;
using challenge.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace challenge.Repositories
{
    public class CompensationRepository : ICompensationRepository
    {
        private readonly EmployeeContext _employeeContext;
        private readonly ILogger<ICompensationRepository> _logger;

        public CompensationRepository(ILogger<ICompensationRepository> logger, EmployeeContext employeeContext)
        {
            _employeeContext = employeeContext;
            _logger = logger;
        }

        public Compensation Create(Compensation compensation)
        {
            compensation.CompensationId = Guid.NewGuid().ToString();
            _employeeContext.Compensations.Add(compensation);
            return compensation;
        }

        public void CreateRelationship(Compensation compensation)
        {
            EmployeeXCompensation employeeXCompensation = new EmployeeXCompensation();
            employeeXCompensation.CompensationId = compensation.CompensationId;
            employeeXCompensation.EmployeeId = compensation.EmployeeId;
            employeeXCompensation.EmployeeXCompensationId = Guid.NewGuid().ToString();
            _employeeContext.EmployeeXCompensations.Add(employeeXCompensation);
        }

        public Compensation GetById(string id)
        {
            Compensation compensation = new Compensation();
            var compensations = _employeeContext.Compensations.Where(x => x.Employee.EmployeeId == id).ToList();
            if (compensations.Count > 1)
            {
                compensation = compensations[0];
                foreach (var comp in compensations)
                {
                    if (comp.EffectiveDate >= compensation.EffectiveDate && comp.EffectiveDate < DateTime.Now) 
                        compensation = comp;
                }
            }
            return compensation;
        }

        public Task SaveAsync()
        {
           return _employeeContext.SaveChangesAsync();
        }
    }
}
