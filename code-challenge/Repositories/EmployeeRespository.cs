﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using challenge.Models;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using challenge.Data;

namespace challenge.Repositories
{
    public class EmployeeRespository : IEmployeeRepository
    {
        private readonly EmployeeContext _employeeContext;
        private readonly ILogger<IEmployeeRepository> _logger;

        public EmployeeRespository(ILogger<IEmployeeRepository> logger, EmployeeContext employeeContext)
        {
            _employeeContext = employeeContext;
            _logger = logger;
        }

        public Employee Add(Employee employee)
        {
            employee.EmployeeId = Guid.NewGuid().ToString();
            _employeeContext.Employees.Add(employee);
            return employee;
        }

        public void AddSuboordinates(EmployeeAddModel ee)
        {
            if (ee.DirectReports != null)
            {
                foreach (var suboordinateId in ee.DirectReports)
                {
                    EmployeeXEmployee employeeX = new EmployeeXEmployee();
                    employeeX.EmployeeXEmployeeId = Guid.NewGuid().ToString();
                    employeeX.EmployeeId = suboordinateId;
                    employeeX.ReportsToId = ee.EmployeeId;
                    _employeeContext.EmployeeXEmployees.Add(employeeX);
                }
            }
        }

        public Employee GetByIdWithReportingStructure(string id)
        {
            Employee ee = _employeeContext.Employees.SingleOrDefault(e => e.EmployeeId == id);
            ee.DirectReports = GetDirectReports(ee);
            return ee;
        }

        public Employee GetById(string id)
        {
            Employee ee = _employeeContext.Employees.SingleOrDefault(e => e.EmployeeId == id);
            return ee;
        }

        public List<Employee>GetDirectReports(Employee ee)
        {
            List<Employee> employees = new List<Employee>();
            var suboordinates =_employeeContext.EmployeeXEmployees.Where(x => x.ReportsToId == ee.EmployeeId).ToList();
            foreach(var suboordinate in suboordinates)
            {
                employees.Add(GetByIdWithReportingStructure(suboordinate.EmployeeId));
            }
            return employees;
        }

        public Task SaveAsync()
        {
            return _employeeContext.SaveChangesAsync();
        }

        public Employee Remove(Employee employee)
        {
            return _employeeContext.Remove(employee).Entity;
        }
    }
}
