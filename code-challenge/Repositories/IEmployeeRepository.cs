﻿using challenge.Models;
using System;
using System.Threading.Tasks;

namespace challenge.Repositories
{
    public interface IEmployeeRepository
    {
        Employee GetById(String id);
        Employee GetByIdWithReportingStructure(String id);
        Employee Add(Employee employee);
        void AddSuboordinates(EmployeeAddModel employee);
        Employee Remove(Employee employee);
        Task SaveAsync();
    }
}