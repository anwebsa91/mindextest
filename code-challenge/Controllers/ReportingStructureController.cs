﻿using challenge.Models;
using challenge.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace challenge.Controllers
{
    [Route("api/reportingstructure")]
    public class ReportingStructureController : Controller
    {
        private readonly ILogger _logger;
        private readonly IReportingStructureService _reportService;
        public ReportingStructureController(ILogger<EmployeeController> logger, IReportingStructureService reportService)
        {
            _logger = logger;
            _reportService = reportService;
        }
        /// <summary>
        /// Returns the reporting structure as defined in the spec for an employee
        /// </summary>
        /// <param name="id">The string id of an employee</param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "getReportingStructure")]
        public IActionResult GetReportingStructure(String id)
        {
            _logger.LogDebug("Getting the reporting structure for " + id);
            ReportingStructure structure = _reportService.GetReportingStructure(id);
            return Ok(structure);
        }
    }
}
