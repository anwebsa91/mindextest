﻿using challenge.Models;
using challenge.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace challenge.Controllers
{
    [Route("api/compensation")]
    public class CompensationController : Controller
    {
        private readonly ILogger _logger;
        private readonly ICompensationService _CompensationService;

        public CompensationController(ILogger<CompensationController> logger, ICompensationService CompensationService)
        {
            _logger = logger;
            _CompensationService = CompensationService;
        }

        [HttpPost]
        public IActionResult CreateCompensation([FromBody] CompensationAddModel compensation)
        {
            _logger.LogDebug($"Received compensation create request for");
            var comp= _CompensationService.Create(compensation);
            return CreatedAtRoute("getCompensationById", new { id = comp.EmployeeId }, comp);
        }
        /// <summary>
        /// Returns a default object if no compensation found
        /// </summary>
        /// <param name="id">The ID of the employee</param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "getCompensationById")]
        public IActionResult GetCompensation(string id)
        {
            var compensation = _CompensationService.GetById(id);

            if (compensation == null)
                return NotFound();

            return Ok(compensation);
        }
    }

}
