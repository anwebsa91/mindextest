﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using challenge.Controllers;
using challenge.Data;
using challenge.Models;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.TestHost;
using code_challenge.Tests.Integration.Extensions;
using System.IO;
using System.Net;
using System.Net.Http;
using code_challenge.Tests.Integration.Helpers;
namespace code_challenge.Tests.Integration
{
    [TestClass]
    public class ReportingStructureControllerTests
    {
        private static HttpClient _httpClient;
        private static TestServer _testServer;

        [ClassInitialize]
        public static void InitializeClass(TestContext context)
        {
            _testServer = new TestServer(WebHost.CreateDefaultBuilder()
                .UseStartup<TestServerStartup>()
                .UseEnvironment("Development"));

            _httpClient = _testServer.CreateClient();
        }

        [ClassCleanup]
        public static void CleanUpTest()
        {
            _httpClient.Dispose();
            _testServer.Dispose();
        }
        [TestMethod]
        public void GetReportingStructure()
        {
            var employeeId = "16a596ae-edd3-4847-99fe-c4518e82c86f";
            var expectedFirstName = "John";
            var expectedLastName = "Lennon";
            var expectedReports= 4;

            var getRequestTask = _httpClient.GetAsync($"api/reportingstructure/{employeeId}");
            var response = getRequestTask.Result;

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            var structure = response.DeserializeContent<ReportingStructure>();
            Assert.AreEqual(expectedFirstName,structure.employee.FirstName);
            Assert.AreEqual(expectedLastName, structure.employee.LastName);
            Assert.AreEqual(expectedReports, structure.numberOfReports);
        }
        //Integration Test
        [TestMethod]
        public void GetReportingStructureWithAnAddition()
        {
            List<String> reportIds = new List<String>();
            reportIds.Add("16a596ae-edd3-4847-99fe-c4518e82c86f");
            var employee = new EmployeeAddModel()
            {
                Department = "Engineering",
                FirstName = "Andy",
                LastName = "Webster",
                Position = "CTO",
                DirectReports = reportIds
            };

            var requestContent = new JsonSerialization().ToJson(employee);

            // Execute
            var postRequestTask = _httpClient.PostAsync("api/employee",
               new StringContent(requestContent, Encoding.UTF8, "application/json"));
            var response = postRequestTask.Result;

            var andy = response.DeserializeContent<Employee>();
            var employeeId = andy.EmployeeId;
            var expectedFirstName = "Andy";
            var expectedLastName = "Webster";
            var expectedReports = 5;

            var getRequestTask = _httpClient.GetAsync($"api/reportingstructure/{employeeId}");
            var response1 = getRequestTask.Result;

            Assert.AreEqual(HttpStatusCode.OK, response1.StatusCode);
            var structure = response1.DeserializeContent<ReportingStructure>();
            Assert.AreEqual(structure.employee.FirstName, expectedFirstName);
            Assert.AreEqual(structure.employee.LastName, expectedLastName);
            Assert.AreEqual(structure.numberOfReports, expectedReports);
        }


    }
}
