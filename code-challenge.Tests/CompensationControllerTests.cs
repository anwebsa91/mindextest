﻿using challenge.Models;
using code_challenge.Tests.Integration.Extensions;
using code_challenge.Tests.Integration.Helpers;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;

namespace code_challenge.Tests.Integration
{
    [TestClass]
    public class CompensationControllerTests
    {
        private static HttpClient _httpClient;
        private static TestServer _testServer;

        [ClassInitialize]
        public static void InitializeClass(TestContext context)
        {
            _testServer = new TestServer(WebHost.CreateDefaultBuilder()
                .UseStartup<TestServerStartup>()
                .UseEnvironment("Development"));

            _httpClient = _testServer.CreateClient();
        }

        [ClassCleanup]
        public static void CleanUpTest()
        {
            _httpClient.Dispose();
            _testServer.Dispose();
        }


        [TestMethod]
        public void AddCompensation()
        {
            CompensationAddModel compensation = new CompensationAddModel
            {
                EffectiveDate = DateTime.Now,
                EmployeeId = "16a596ae-edd3-4847-99fe-c4518e82c86f",
                Salary = 20000
            };

            var requestContent = new JsonSerialization().ToJson(compensation);
            // Execute
            var postRequestTask = _httpClient.PostAsync("api/compensation",
               new StringContent(requestContent, Encoding.UTF8, "application/json"));
            var response = postRequestTask.Result;

            // Assert
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);

            var newCompensation = response.DeserializeContent<Compensation>();
            Assert.IsNotNull(newCompensation.EmployeeId);
            Assert.IsNotNull(newCompensation.Employee);
            Assert.AreEqual(compensation.EffectiveDate, newCompensation.EffectiveDate);
            Assert.AreEqual(compensation.Salary, newCompensation.Salary);
            Assert.AreEqual("John", newCompensation.Employee.FirstName);
            Assert.AreEqual("Lennon", newCompensation.Employee.LastName);
        }


        [TestMethod]
        public void ReturnCompensation()
        {
            CompensationAddModel compensation = new CompensationAddModel
            {
                EffectiveDate = DateTime.Now,
                EmployeeId = "16a596ae-edd3-4847-99fe-c4518e82c86f",
                Salary = 20000
            };

            var requestContent = new JsonSerialization().ToJson(compensation);
            // Execute
            var postRequestTask = _httpClient.PostAsync("api/compensation",
               new StringContent(requestContent, Encoding.UTF8, "application/json"));
            var response = postRequestTask.Result;

            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
            var newCompensation = response.DeserializeContent<Compensation>();

            var getRequestTask = _httpClient.GetAsync($"api/compensation/{newCompensation.EmployeeId}");
            var response1 = getRequestTask.Result;

            Assert.AreEqual(HttpStatusCode.OK, response1.StatusCode);
            var structure = response1.DeserializeContent<Compensation>();

            Assert.AreEqual(compensation.EmployeeId, structure.EmployeeId);
            Assert.AreEqual(compensation.Salary, structure.Salary);
            Assert.AreEqual(compensation.EffectiveDate, structure.EffectiveDate);
        }

        [TestMethod]
        public void NullObjectReturned()
        {
            var getRequestTask = _httpClient.GetAsync($"api/compensation/randomstring");
            var response1 = getRequestTask.Result;
            var structure = response1.DeserializeContent<Compensation>();
            Assert.IsNotNull(structure);
            Assert.IsNull(structure.Employee);
        }
    }
}
